#include <Wire.h>
#include "PacketTool.h"

#define ENCODER_FL 8
#define ENCODER_FR 11
#define ENCODER_RL 9
#define ENCODER_RR 10

#define MAX_SPEED 125

double2bytes_t encoderPOS;
double2bytes_t rpm;

double temp;

//returns the requested encoders position
double requestEncoderPOS(int encoderNumber){
  double2bytes_t encoderPOS;
  //send request to specified slave
  Wire.requestFrom(encoderNumber, PACKET_SIZE);
  //read the first 4 bytes of response to get position
  int i = 0;
  while (i<4 && Wire.available()){
    encoderPOS.b[i] = Wire.read();
    i++;
  }

  //empty the wire buffer so it doesn't get mistakenly read later
  while (Wire.available()){
    Wire.read();
  }

  return encoderPOS.d;
}

//request the specified encoder's RPM
double requestEncoderRPM(int encoderNumber){
  double2bytes_t encoderRPM;

  Wire.requestFrom(encoderNumber, PACKET_SIZE);

  int i = 0;
  while(i<4 && Wire.available()){
    Wire.read();
    i++;
  }
  // i=0;
  // while(i<4 && Wire.available()){
  //   encoderRPM.b[i] = Wire.read();
  //   i++;
  // }

  return encoderPOS.d;
}

void setup() {
  Wire.begin();
  Serial.begin(9600);
  Serial.println("READY");
}

void loop() {
  if(requestEncoderPOS(ENCODER_FL)<1800){
    FL->setSpeed(125);
    FL->run(FORWARD);
  } else {
    FL->run(RELEASE);
  }

  if(requestEncoderPOS(ENCODER_FR)<1800){
    FR->setSpeed(125);
    FR->run(FORWARD);
  } else {
    FR->run(RELEASE);
  }

  if(requestEncoderPOS(ENCODER_RL)<1800){
    RL->setSpeed(125);
    RL->run(FORWARD);
  } else {
    RL->run(RELEASE);
  }

  if(requestEncoderPOS(ENCODER_RR)<1800){
    RR->setSpeed(125);
    RR->run(FORWARD);
  } else {
    RR->run(RELEASE);
  }

}
