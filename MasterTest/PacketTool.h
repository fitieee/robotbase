

#define PACKET_SIZE 8 //4-long, 4-double

//All variables are just bytes to the computer, when you retrieve a variable
//the program knows how many bytes that variable is and grabs the right peices
//This type union means that the program can read the same memory locations as
//two different types. The long or other variable can be accesed byte by byte
//in a byte array. No conversion nessicary.


typedef union long2bytes_t
 // union consists of one variable represented in a number of different ways
{
  long l;
  //uint8_t is just a fancy way to say this is a byte
  uint8_t b[4];
};


typedef union double2bytes_t
// union consists of one variable represented in a number of different ways
{
  double d;
  //uint8_t is just a fancy way to say this is a byte
  uint8_t b[4];
};
