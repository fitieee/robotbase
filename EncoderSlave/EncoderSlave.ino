#include <Encoder.h>
#include <Wire.h>
#include "PacketTool.h"

#define INTERVAL 20.0

//initialize the encoder using pins 2 & 3 becuase they are the only
//ones that can be used as hardware interupts.
Encoder e(2,3);

double2bytes_t currentPOS;
double previousPOS = 0;
double2bytes_t rpm;
unsigned long previousTime = 0;
unsigned long currentTime;

// this function is called when this device receives a
// request from the master device
void requestEvent(){
  Wire.write(currentPOS.b, 4);
}


void setup() {
  //FL -> 8
  //FR -> 11
  //RL -> 9
  //RR -> 10
  Wire.begin(8);
  Wire.onRequest(requestEvent);
  Serial.begin(9600);
  Serial.println("READY");
}

void loop() {

  currentPOS.d = (double)e.read();
//   currentTime = millis();
// //  Serial.println(currentPOS);
//
//   if(currentTime - previousTime > INTERVAL){
//     previousTime = currentTime;
//     rpm.d = ((currentPOS.d - previousPOS)*60.0)/INTERVAL;
//     previousPOS = currentPOS.d;
//   }

  Serial.println(currentPOS.b[0]);
  Serial.println(currentPOS.b[1]);
  Serial.println(currentPOS.b[2]);
  Serial.println(currentPOS.b[3]);
  Serial.println(currentPOS.d);
  Serial.println("______________");

  delay(250);
}
